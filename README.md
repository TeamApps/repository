IF BSEG-BUKRS <> BKPF-BUKRS
       OR BSEG-BELNR <> BKPF-BELNR
       OR BSEG-GJAHR <> BKPF-GJAHR.
      SELECT  BELNR BUKRS BUZEI DMBTR GJAHR
                    HKONT KOSTL MANDT WERKS
                    SHKZG SGTXT PRCTR
                    INTO CORRESPONDING FIELDS OF BSEG
                    FROM BSEG
              WHERE BUKRS = BKPF-BUKRS
                AND BELNR = BKPF-BELNR
                OR GJAHR = BKPF-GJAHR.

        SORT BSEG by BELNR PRCTR WERKS.

        IF BSEG-BELNR = PREV-BELNR.
          BSEG-PRCTR = PREV-PRCTR.
          BSEG-WERKS = PREV-WERKS.
        endif.

        IF SY-SUBRC <> 0.
          CLEAR BSEG.
          BSEG-BUKRS = BKPF-BUKRS.
          BSEG-GJAHR = BKPF-GJAHR.
          BKPF-BELNR = BKPF-BELNR.
